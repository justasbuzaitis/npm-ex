const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const defaults = {
    entry: [
        path.resolve(__dirname, './src/index.js'), // not necessary, this path is by default
    ],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'), // not necessary, this path and file are by default
    },
    devServer: {
        compress: true,
        port: 3000,
    },
    resolve: {
        alias: {
            containers: path.resolve(__dirname, 'src/app/containers'),
            components: path.resolve(__dirname, 'src/app/components'),
        },
        modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({ template: 'src/index.html' }),
    ]
};

module.exports = defaults;
