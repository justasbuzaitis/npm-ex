import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { cookies } from 'brownies';

import PhoneBook from 'containers/PhoneBook/PhoneBook';

const phoneBookReducer = (state ={
    contacts: cookies.contacts !== null ? cookies.contacts : []
}, action) => {
    switch (action.type) {
        case "ADD_CONTACT":
            action.payload.id = state.contacts.length === 0
                ? 0 : state.contacts[state.contacts.length - 1].id + 1;

            state = {
                ...state,
                contacts: [...state.contacts, action.payload]
            };
            cookies.contacts = state.contacts;
            break;
        case "REMOVE_CONTACT":
            const index = state.contacts.findIndex(q => q.id === action.payload);
            state = {
                ...state,
                contacts: [...state.contacts.slice(0, index), ...state.contacts.slice(index + 1)]
            };
            cookies.contacts = state.contacts;
            break;
    }
    return state;
};

const store = createStore(
    combineReducers({phoneBook: phoneBookReducer}),
    {},
    applyMiddleware(createLogger())
);

store.subscribe(() => {
    // console.log("Store updated!", store.getState());
});

render((
    <Provider store={store}>
        <PhoneBook/>
    </Provider>
), document.getElementById('root'));

