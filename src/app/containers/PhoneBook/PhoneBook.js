import React from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter } from 'react-router-dom';
import { connect } from "react-redux";

import PhoneBookGenerator from 'app/components/PhoneBookGenerator/PhoneBookGenerator';
import AllContacts from 'app/components/AllContacts/AllContacts';

class PhoneBook extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <HashRouter>
                <div>
                    <nav
                        className="navbar navbar-expand-lg navbar-light bg-light"
                    >
                        <div
                            className="collapse navbar-collapse"
                            id="navbarSupportedContent"
                        >
                            <ul
                                className="navbar-nav mr-auto"
                            >
                                <li
                                    className="nav-item"
                                >
                                    <Link to="/generator" className="nav-link"> Phone book generator </Link>
                                </li>
                                <li
                                    className="nav-item"
                                >
                                    <Link to="/all" className="nav-link"> All contacts </Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <hr />
                    <div
                        className="mt-5 px-5 row"
                    >
                        <Route path="/generator" render={() => (
                            <PhoneBookGenerator
                                contacts={this.props.phoneBook.contacts}
                                removeContact={this.props.removeContact}
                                addContact={this.props.addContact}
                            />
                        )}/>
                        <Route path="/all" render={() => (
                            <AllContacts
                                contacts={this.props.phoneBook.contacts}
                                removeContact={this.props.removeContact}
                            />
                        )}/>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        phoneBook: state.phoneBook
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addContact: (contact) => {
            dispatch({
                type: "ADD_CONTACT",
                payload: contact
            })
        },
        removeContact: (id) => {
            dispatch({
                type: "REMOVE_CONTACT",
                payload: id
            })
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneBook);
