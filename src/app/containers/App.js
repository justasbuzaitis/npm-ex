class App {
    addresses = [];

    constructor() {
        const form = document.getElementById('form');
        const addressList = document.getElementById('list-tab');

        form.addEventListener('submit', this.renderAddress);
        addressList.addEventListener('click', this.renderPhone);
    }


    renderAddress = (e) => {
        e.preventDefault();

        const address = {
            id: this.getNewAddressId(this),
            name: document.querySelector('#name').value,
            phone: document.querySelector('#phone').value,
        };

        this.addresses.push(address);


        const addressElement = document.createElement('a');

        addressElement.setAttribute('class', 'list-group-item list-group-item-action');
        addressElement.setAttribute('data-toggle', 'list');
        addressElement.setAttribute('role', 'tab');
        addressElement.setAttribute('id', address.id);
        addressElement.text = address.name;

        document.getElementById('list-tab').appendChild(addressElement);
    };

    getNewAddressId = () => {
        return this.addresses.length === 0
            ? 0
            : this.addresses[this.addresses.length - 1].id + 1;
    };

    renderPhone = (e) => {
        this.showPhoneById(e.target.id);
    };

    showPhoneById = (id) => {
        document.getElementById('phoneOut').innerText = this.addresses[id].phone;
    };
}

export default App;
