import React from 'react';

import ContactForm from 'app/components/ContactForm/ContactForm';
import ContactsForm from 'app/components/ContactsForm/ContactsForm';
import ContactCardDialog from 'app/components/ContactCardDialog/ContactCardDialog';

class PhoneBookGenerator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            openDialog: false,
            dialogContent: '',
            dialogTitle: '',
            dialogImage: '',
            dialogId: null,
        };

        console.log(this.props.test);
    }

    handleCloseDialog = () => {
        this.setState({ openDialog: false });
    };

    handleOpenDialog = (id, title, content, image) => {
        //const index = this.props.contacts.findIndex(q => q.id === id);
        this.setDialogTitle(`name: ${title}`);
        this.setDialogContent(`phone: ${content}`);
        this.setDialogImage(image);
        this.setDialogId(id);
        this.setState({ openDialog: true });
    };

    setDialogTitle = (title) => {
        this.setState({ dialogTitle: title });
    };

    setDialogContent = (content) => {
        this.setState({ dialogContent: content });
    };

    setDialogImage = (image) => {
        this.setState({ dialogImage: image });
    };

    setDialogId = (id) => {
        this.setState({ dialogId: id });
    };

    render() {
        return (
            <React.Fragment>
                <ContactForm
                    addContact={this.props.addContact}
                />
                <ContactsForm
                    contacts={this.props.contacts}
                    handleOpenDialog={this.handleOpenDialog}
                />
                <ContactCardDialog
                    removeContact={this.props.removeContact}
                    handleCloseDialog={this.handleCloseDialog}
                    openDialog={this.state.openDialog}
                    id={this.state.dialogId}
                    title={this.state.dialogTitle}
                    content={this.state.dialogContent}
                    image={this.state.dialogImage}
                />
            </React.Fragment>
        );
    }
}

export default PhoneBookGenerator;
