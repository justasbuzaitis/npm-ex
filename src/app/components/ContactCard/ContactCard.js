import React from 'react';
import PropTypes from 'prop-types';

export default class ContactCard extends React.Component{
    handleRemoveContact = (e) => {
        e.preventDefault();
        const id = this.props.id;
        this.props.removeContact(id);
    };

    render() {
        return(
            <div>
                <img
                    className="card-img-top"
                    src={this.props.image}
                    alt="contact"
                />
                <div
                    className="card-body"
                >
                    <h5
                        className="card-title"
                    >
                        {this.props.title}
                    </h5>
                    <p
                        className="card-text"
                    >
                        {this.props.content}
                    </p>
                    <a
                        href={this.props.href}
                        className="btn btn-primary"
                    >
                        Call
                    </a>
                    <a
                        id={this.props.id}
                        onClick={this.handleRemoveContact}
                        className="btn btn-danger"
                    >
                        Delete
                    </a>
                </div>
            </div>
        );
    }
}

ContactCard.propTypes = {
    removeContact: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
};
