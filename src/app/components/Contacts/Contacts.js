import React from 'react';
import PropTypes from 'prop-types';

class Contacts extends React.Component {
    handleOpenDialog = (e) => {
        e.preventDefault();
        const index = this.props.contacts.findIndex(q => q.id === Number(e.target.id));
        const contact = this.props.contacts[index];
        this.props.handleOpenDialog(contact.id, contact.name, contact.phone, contact.image);
    };

    render() {
        const { contacts } = this.props;

        return (
            contacts.map(contact => (
                <a
                    className="list-group-item list-group-item-action"
                    data-toggle="list"
                    role="tab"
                    id={contact.id}
                    onClick={this.handleOpenDialog}
                    onKeyDown={this.handleOpenDialog}
                >
                    {contact.name}
                </a>
            ))
        );
    }
}

Contacts.propTypes = {
    contacts: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            phone: PropTypes.number.isRequired,
            image: PropTypes.string.isRequired,
        }),
    ).isRequired,
    handleOpenDialog: PropTypes.func.isRequired,
};

export default Contacts;
