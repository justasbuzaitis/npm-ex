import React from 'react';
import Contacts from 'app/components/Contacts/Contacts';
import PropTypes from 'prop-types';

class ContactsForm extends React.Component {
    render() {
        return (
            <div
                className="col-md-4"
            >
                <label
                    htmlFor="list-tab"
                >
                    Contacts
                </label>
                <div
                    id="list-tab"
                    className="list-group"
                    role="tablist"
                >
                    <Contacts
                        contacts={this.props.contacts}
                        handleOpenDialog={this.props.handleOpenDialog}
                    />
                </div>
            </div>
        );
    }
}

ContactsForm.propTypes = {
    contacts: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            phone: PropTypes.number.isRequired,
        }),
    ).isRequired,
    handleOpenDialog: PropTypes.func.isRequired,
};

export default ContactsForm;
