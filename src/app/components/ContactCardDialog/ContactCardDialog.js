import React from 'react';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import Dialog from '@material-ui/core/Dialog/Dialog';
import PropTypes from 'prop-types';

import ContactCard from 'app/components/ContactCard/ContactCard';

class ContactCardDialog extends React.Component {
    handleRemoveContact = (id) => {
        this.props.handleCloseDialog();
        this.props.removeContact(id);
    };

    render() {
        const href = `tel:${this.props.content}`;

        return (
            <Dialog
                open={this.props.openDialog}
                onClose={this.props.handleCloseDialog}
                scroll="paper"
                aria-labelledby="scroll-dialog-title"
            >
                <DialogContent>
                    <ContactCard
                        id={this.props.id}
                        image={this.props.image}
                        title={this.props.title}
                        content={this.props.content}
                        href={href}
                        removeContact={this.handleRemoveContact}
                    />
                </DialogContent>
            </Dialog>
        );
    }
}

ContactCardDialog.propTypes = {
    handleCloseDialog: PropTypes.func.isRequired,
    removeContact: PropTypes.func.isRequired,
    content: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    openDialog: PropTypes.bool.isRequired,
};

export default ContactCardDialog;
