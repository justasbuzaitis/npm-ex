import React from 'react';
import ContactCard from "app/components/ContactCard/ContactCard";
import PropTypes from "prop-types";

export default class AllContacts extends React.Component {
        render() {
        const contacts = this.props.contacts;
        console.log(contacts);
        return (
            contacts.map(contact => (
                <ContactCard
                    id={contact.id}
                    image={contact.image}
                    title={`name: ${contact.name}`}
                    content={`phone: ${contact.phone}`}
                    href={`tel:${contact.phone}`}
                    removeContact={this.props.removeContact}
                />
            ))
        );
    }
}

AllContacts.propTypes = {
    removeContact: PropTypes.func.isRequired,
    contacts: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            phone: PropTypes.number.isRequired,
            image: PropTypes.string.isRequired,
        }),
    ).isRequired,
};
