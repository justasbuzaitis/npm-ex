import React from 'react';
import PropTypes from 'prop-types';

class ContactForm extends React.Component {
    handleAddContact = (e) => {
        e.preventDefault();

        this.props.addContact({
            name: document.querySelector('#name').value,
            phone: document.querySelector('#phone').value,
            image: `https://picsum.photos/200/200/?image=${Math.floor(Math.random() * 200)}`,
        });
    };

    render() {
        return (
            <form
                id="form"
                className="col-md-4"
                onSubmit={this.handleAddContact}
            >
                <div className="form-group">
                    <label
                        htmlFor="name"
                    >
                        Name
                    </label>
                    <input
                        id="name"
                        type="text"
                        className="form-control"
                        name="name"
                        autoComplete="off"
                    />
                </div>
                <div className="form-group">
                    <label
                        htmlFor="phone"
                    >
                        Phone number:
                    </label>

                    <input
                        id="phone"
                        type="tel"
                        placeholder="+4407911123456"
                        pattern="[\+]\d{8,15}"
                        className="form-control"
                        name="phone"
                        autoComplete="off"
                    />
                </div>
                <button
                    type="submit"
                    id="submit"
                    className="btn btn-primary"
                >
                    Add
                </button>
            </form>
        );
    }
}

ContactForm.propTypes = {
    addContact: PropTypes.func.isRequired,
};

export default ContactForm;
