FROM node:10.12.0

RUN npm install webpack -g
RUN npm install webpack-cli -g

WORKDIR /tmp
COPY package.json /tmp/
RUN npm config set registry http://registry.npmjs.org/ && npm install

WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN cp -a /tmp/node_modules /usr/src/app/
RUN webpack

CMD [ "node", "server" ]
EXPOSE 3000